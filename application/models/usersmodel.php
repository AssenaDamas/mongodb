<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usersmodel extends CI_Model {

	function getUsers($id) {

		return $this->mongo_db->where(array('_id' => new mongoId($id)))->get('mahasiswa');
		
	}

	function insertUsers() {

		$data = array(
					'nim'		=> $this->input->post('nim'),
					'nama' 	    => $this->input->post('nama'),
					'prodi' 	=> $this->input->post('prodi'), 
					'alamat' 	=> $this->input->post('alamat'),
					'phone' 	=> $this->input->post('phone')
					);

		$this->mongo_db->insert('mahasiswa', $data);
		return true; 

	}

	function editProcess($id) {

		$data = array(
				'nim'		=> $this->input->post('nim'),
				'nama' 		=> $this->input->post('nama'),
				'prodi' 	=> $this->input->post('prodi'), 
				'alamat' 	=> $this->input->post('alamat'), 
				'phone' 	=> $this->input->post('phone')
			);
		// print_r($data);
		// exit();

		// $this->mongo_db->where(array('_id' => new mongoId($id)));
		// return $this->mongo_db->update('mahasiswa', $data);
		
		return $this->mongo_db->where(array('_id'=>new mongoId($id)))->set($data)->update('mahasiswa');

	} 

	function viewUsers() {

		return $this->mongo_db->get('mahasiswa');

	}

	function deleteUser($id) {

		return $this->mongo_db->where(array('_id' => new mongoId($id)))->delete('mahasiswa');
	}
	

}

/* End of file usersmodel.php */
/* Location: ./application/models/usersmodel.php */